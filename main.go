package main

import (
	"flag"

	"gitlab.com/nicolebroyak/go-webchat-gin-vue/db"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/server"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/wswebsocket"
)

func main() {
	seedFlag := flag.Bool("s", false, "run seeding script")
	flag.Parse()

	if *seedFlag {
		db.SeedDatabase(db.Conn)
		return
	}

	wsManager := wswebsocket.NewManager()

	router := server.SetupRouter(wsManager)
	router.Run()

}
