//go:build !prod
// +build !prod

package main

import (
	"log"
	"net/http"
	"os"
)

func getFrontendAssets() http.FileSystem {
	log.Println("DEV")
	return http.FS(os.DirFS("dist"))
}
