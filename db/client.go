package db

import (
	"fmt"

	"github.com/caarlos0/env/v7"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/dbmodels"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/logger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type dbConfig struct {
	User     string `env:"DB_USER"`
	Password string `env:"DB_PASSWORD"`
	Host     string `env:"DB_HOST"`
	Port     string `env:"DB_PORT"`
	Name     string `env:"DB_NAME"`
}

var Conn *gorm.DB

func init() {
	cfg := &dbConfig{}
	opts := env.Options{RequiredIfNoDef: true}

	if err := env.Parse(cfg, opts); err != nil {
		logger.L.Fatal(err)
	}

	dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=%v sslmode=disable",
		cfg.Host, cfg.User, cfg.Password, cfg.Name, cfg.Port,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		logger.L.Fatal(err)
	}

	err = db.AutoMigrate(dbmodels.User{}, dbmodels.Chat{}, dbmodels.Message{})
	if err != nil {
		logger.L.Fatal(err)
	}

	Conn = db
}
