package db

import (
	"fmt"

	"gitlab.com/nicolebroyak/go-webchat-gin-vue/dbmodels"
	"gorm.io/gorm"
)

func SeedDatabase(dbconn *gorm.DB) {
	users := dbmodels.Users{
		&dbmodels.User{
			Nickname:             "Niko",
			Password:             "1111",
			PasswordConfirmation: "1111",
			Email:                "niko@xd.pl",
		},
		&dbmodels.User{
			Nickname:             "Geecko",
			Password:             "1111",
			PasswordConfirmation: "1111",
			Email:                "nikola@geecko.la",
		},
		&dbmodels.User{
			Nickname:             "Nikola",
			Password:             "1111",
			PasswordConfirmation: "1111",
			Email:                "nikolabroyak@gmail.com",
		},
	}

	for i := 0; i < 10; i++ {
		user := &dbmodels.User{
			Nickname:             fmt.Sprintf("TestUser#%v", i),
			Password:             "1111",
			PasswordConfirmation: "1111",
			Email:                fmt.Sprintf("testuser%v@gmail.com", i),
		}

		users = append(users, user)
	}

	dbconn.Create(users)

	users = dbmodels.Users{}
	dbconn.Limit(3).Find(&users)
	chats := dbmodels.Chats{
		&dbmodels.Chat{
			Name:  "niko & geecko",
			Users: dbmodels.Users{users[0], users[1]},
		},
		&dbmodels.Chat{
			Name:  "niko & nikola",
			Users: dbmodels.Users{users[0], users[2]},
		},
		&dbmodels.Chat{
			Name:  "nikola & geecko",
			Users: dbmodels.Users{users[2], users[1]},
		},
	}
	dbconn.Create(chats)

	users = dbmodels.Users{}
	dbconn.Preload("Chats").Limit(3).Find(&users)
	chats = dbmodels.Chats{}
	dbconn.Preload("Users").Find(&chats)

	messages := dbmodels.Messages{
		&dbmodels.Message{
			Value:  "test message",
			UserID: users[0].ID,
			ChatID: chats[0].ID,
		},
		&dbmodels.Message{
			Value:  "test message 2",
			UserID: users[2].ID,
			ChatID: chats[2].ID,
		},
		&dbmodels.Message{
			Value:  "test message 3",
			UserID: users[1].ID,
			ChatID: chats[0].ID,
		},
		&dbmodels.Message{
			Value:  "test message 4",
			UserID: users[2].ID,
			ChatID: chats[1].ID,
		},
		&dbmodels.Message{
			Value:  "test message 5",
			UserID: users[0].ID,
			ChatID: chats[1].ID,
		},
		&dbmodels.Message{
			Value:  "test message 6",
			UserID: users[1].ID,
			ChatID: chats[2].ID,
		},
	}

	dbconn.Create(messages)
}
