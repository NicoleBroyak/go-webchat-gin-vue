//go:build prod
// +build prod

package main

import (
	"embed"
	"io/fs"
	"log"
	"net/http"
)

//go:embed dist
var embedFrontend embed.FS

func getFrontendAssets() http.FileSystem {
	log.Println("PROD")
	f, err := fs.Sub(embedFrontend, "dist")
	if err != nil {
		panic(err)
	}

	return http.FS(f)
}
