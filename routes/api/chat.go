package api

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/db"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/dbmodels"
)

func ChatsHandler(c *gin.Context) {
	offset, _ := strconv.Atoi(c.Param("offset"))
	limit, _ := strconv.Atoi(c.Param("limit"))
	chats := dbmodels.Chats{}

	db.Conn.Offset(offset).Limit(limit).Find(&chats)

	c.JSON(http.StatusOK, chats)
}
