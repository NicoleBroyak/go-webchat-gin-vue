package api

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/db"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/dbmodels"
)

func MessagesHandler(c *gin.Context) {
	chatID := c.Param("chat_id")
	offset, _ := strconv.Atoi(c.Param("offset"))
	limit, _ := strconv.Atoi(c.Param("limit"))
	messages := dbmodels.Messages{}

	db.Conn.Where("chat_id = ?", chatID).Preload("User").Offset(offset).Limit(limit).Find(&messages)

	for i, _ := range messages {
		messages[i].User.PasswordHash = ""
	}

	c.JSON(http.StatusOK, messages)
}
