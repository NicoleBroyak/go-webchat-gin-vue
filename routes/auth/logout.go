package auth

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/logger"
)

func LogoutHandler(c *gin.Context) {
	logger.L.Info("LOGOUT")
	c.SetCookie("accessToken", "", -1, "/", "", false, true)
	c.SetCookie("refreshToken", "", -1, "/", "", false, false)
	logger.L.Info("LOGOUT")
	c.HTML(http.StatusOK, "index.html", nil)
}
