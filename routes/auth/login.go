package auth

import (
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/db"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/dbmodels"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/jwtauth"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/logger"
	"golang.org/x/crypto/bcrypt"
)

func LoginHandler(c *gin.Context) {
	user := dbmodels.User{}
	if c.ShouldBind(&user) == nil {
		logger.L.Info(user.Email)
		logger.L.Info(user.Password)
	}
	// find a user with the email
	result := db.Conn.Where("email = ?", strings.ToLower(strings.TrimSpace(user.Email))).First(&user)
	if result.Error != nil {
		logger.L.Error(result.Error)
		return
	}

	err := bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(user.Password))
	if err != nil {
		logger.L.Error(err)
		return
	}

	logger.L.Info(user.ID)

	tokensMap, err := jwtauth.GenerateJWTPair(user)
	c.SetCookie("accessToken", tokensMap["accessToken"], 900, "/", "", false, true)
	c.SetCookie("refreshToken", tokensMap["refreshToken"], 3600*24, "/", "", false, false)

	c.Redirect(301, "/")
}
