package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func IndexHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", nil)
}

func PingHandler(c *gin.Context) {
	c.String(http.StatusOK, "pong")
}
