package dbmodels

import (
	"gorm.io/gorm"
)

type Message struct {
	gorm.Model
	UserID       uint `json:"user_id"`
	User         User
	ChatID       uint `json:"chat_id"`
	Chat         Chat
	Value        string `json:"value"`
	TimeFomatted string `json:"time_formatted" gorm:"-"`
}

type Messages []*Message
