package dbmodels

import (
	"strings"

	"github.com/pkg/errors"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Email        string `json:"email" gorm:"unique" form:"email"`
	Nickname     string `json:"nickname" gorm:"unique" form:"nickname"`
	PasswordHash string `json:"password_hash"`

	Password             string `json:"-" gorm:"-" form:"password"`
	PasswordConfirmation string `json:"-" gorm:"-" form:"passwordconfirmation`
	Messages             Messages
	Chats                Chats `gorm:"many2many:users_chats;"`
}

type Users []*User

func (u *User) BeforeCreate(db *gorm.DB) error {
	switch {
	case u.Email == "":
		return db.AddError(errors.New("empty email"))
	case u.Nickname == "":
		return db.AddError(errors.New("empty nickname"))
	}
	u.Email = strings.ToLower(u.Email)
	u.Nickname = strings.ToLower(u.Nickname)
	ph, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return errors.WithStack(err)
	}
	u.PasswordHash = string(ph)
	if u.PasswordHash == "" {
		return db.AddError(errors.New("empty password hash"))
	}
	return nil
}
