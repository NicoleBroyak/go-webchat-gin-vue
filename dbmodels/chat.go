package dbmodels

import "gorm.io/gorm"

type Chat struct {
	gorm.Model
	Name     string `json:"name"`
	Users    Users  `gorm:"many2many:users_chats;"`
	Messages Messages
}

// Chats is not required by pop and may be deleted
type Chats []*Chat
