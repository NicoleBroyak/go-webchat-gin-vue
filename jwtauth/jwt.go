package jwtauth

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/caarlos0/env/v7"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/db"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/dbmodels"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/logger"
)

type jwtcfg struct {
	Secret string `env:"JWTSECRET"`
}

var cfg jwtcfg

func init() {
	opts := env.Options{}
	if err := env.Parse(&cfg, opts); err != nil {
		logger.L.Fatal(err)
	}
	logger.L.Info(cfg.Secret)
}

func GenerateJWTPair(user dbmodels.User) (map[string]string, error) {
	accessToken := jwt.New(jwt.SigningMethodHS512)
	claims := accessToken.Claims.(jwt.MapClaims)
	claims["exp"] = time.Now().Add(15 * time.Minute).Unix()
	claims["authorized"] = true
	claims["user"] = user.Nickname
	claims["id"] = user.ID

	accessTokenString, err := accessToken.SignedString([]byte(cfg.Secret))
	if err != nil {
		return nil, err
	}
	logger.L.Info(accessTokenString)

	refreshToken := jwt.New(jwt.SigningMethodHS512)
	refreshTokenClaims := refreshToken.Claims.(jwt.MapClaims)
	refreshTokenClaims["exp"] = time.Now().Add(24 * time.Hour).Unix()
	refreshTokenClaims["id"] = user.ID

	refreshTokenString, err := refreshToken.SignedString([]byte(cfg.Secret))
	if err != nil {
		return nil, err
	}
	return map[string]string{
		"accessToken":  accessTokenString,
		"refreshToken": refreshTokenString,
	}, nil
}

func VerifyJWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		if tokenCookie, _ := c.Cookie("accessToken"); tokenCookie != "" {
			validateTokenFromCookie(c, tokenCookie)
		} else if refreshToken, _ := c.Cookie("refreshToken"); refreshToken != "" {
			token := validateTokenFromCookie(c, refreshToken)
			verifyRefreshToken(c, token)
		} else {
			c.Writer.WriteHeader(http.StatusUnauthorized)
			c.Redirect(301, "/")
		}
	}
}

func validateTokenFromCookie(c *gin.Context, tokenCookie string) *jwt.Token {
	token, err := jwt.Parse(tokenCookie, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		logger.L.Info(ok, token.Method)
		if !ok {
			logger.L.Info("REDIR")
			c.Writer.WriteHeader(http.StatusUnauthorized)
			c.Redirect(301, "/")
		}
		return []byte(cfg.Secret), nil

	})

	logger.L.Info(err)
	if err != nil {
		c.Writer.WriteHeader(http.StatusUnauthorized)
		c.Redirect(301, "/")
	}

	if !token.Valid {
		logger.L.Info("REDIR")
		c.Writer.WriteHeader(http.StatusUnauthorized)
		c.Redirect(301, "/")
	}

	return token

}

func verifyRefreshToken(c *gin.Context, token *jwt.Token) {
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return
	}

	userID, err := strconv.Atoi(fmt.Sprintf("%v", claims["id"]))
	if err != nil {
		return
	}

	user := dbmodels.User{}
	res := db.Conn.First(&user, userID)
	if res.Error != nil {
		logger.L.Error(res.Error)
		return
	}

	tokensMap, err := GenerateJWTPair(user)
	if err != nil {
		logger.L.Error(err)
		return
	}

	c.SetCookie("accessToken", tokensMap["accessToken"], 900, "/", "", false, true)
	c.SetCookie("refreshToken", tokensMap["refreshToken"], 3600*24, "/", "", false, false)
	return
}
