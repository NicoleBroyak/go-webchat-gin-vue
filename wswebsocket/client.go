package wswebsocket

import (
	"encoding/json"
	"log"
	"time"

	"github.com/gorilla/websocket"
)

var (
	pongWait = 60 * time.Second

	pingInterval = (pongWait * 9) / 10
)

type ClientList map[*Client]bool

type Client struct {
	connection *websocket.Conn
	manager    *Manager

	userID        string
	chatIDs       []string
	currentChatID string

	egress chan Event
}

// func NewClient(userID string, conn *websocket.Conn, manager *Manager) *Client {
//
//		c := &Client{
//			connection: conn,
//			userID:     userID,
//			manager:    manager,
//			egress:     make(chan Event),
//		}
//
//		tx := models.DB
//
//		userChatJunctions := models.UserChatJunctions{}
//		err := tx.Where("user_id = ?", userID).All(&userChatJunctions)
//		if err != nil {
//			logger.L.Error(err)
//			return nil
//		}
//
//		userChatsIDs := []string{}
//
//		for _, userChatJunction := range userChatJunctions {
//			userChatsIDs = append(userChatsIDs, userChatJunction.ChatID.String())
//		}
//
//		logger.L.Info(userChatsIDs)
//		logger.L.Info(userID)
//
//		chats := models.Chats{}
//
//		err = tx.Where("id in (?)", userChatsIDs).All(&chats)
//
//		for _, chat := range chats {
//			c.chatIDs = append(c.chatIDs, chat.ID.String())
//		}
//
//		logger.L.Info(c.chatIDs)
//		return c
//	}
func (c *Client) readMessages() {
	defer func() {
		c.manager.removeClient(c)
	}()

	if err := c.connection.SetReadDeadline(time.Now().Add(pongWait)); err != nil {
		log.Println(err)
		return
	}

	c.connection.SetReadLimit(1024)

	c.connection.SetPongHandler(c.pongHandler)

	for {
		_, payload, err := c.connection.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Println(err)
			}
			break
		}

		var request Event

		if err := json.Unmarshal(payload, &request); err != nil {
			log.Println("error marshaling event: ", err)
			break
		}

		if err := c.manager.routeEvent(request, c); err != nil {
			log.Println("error handling message: ", err)
		}
	}
}

func (c *Client) writeMessages() {
	defer func() {
		c.manager.removeClient(c)
	}()

	ticker := time.NewTicker(pingInterval)

	for {
		select {
		case message, ok := <-c.egress:
			if !ok {
				if err := c.connection.WriteMessage(websocket.CloseMessage, nil); err != nil {
					log.Println("connection closed: ", err)
				}

			}

			data, err := json.Marshal(message)
			if err != nil {
				log.Println("error marshalling  message: ", err)
				return
			}

			if err := c.connection.WriteMessage(websocket.TextMessage, data); err != nil {
				log.Println("failed to send message: ", err)
			}
			log.Println("message sent")
		case <-ticker.C:
			log.Println("ping")

			if err := c.connection.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				log.Print("writemsg err: ", err)
				return
			}
		}
	}
}

func (c *Client) pongHandler(pongMsg string) error {
	log.Println("pong")
	return c.connection.SetReadDeadline(time.Now().Add(pongWait))
}
