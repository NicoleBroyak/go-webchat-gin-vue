package wswebsocket

import (
	"encoding/json"
	"time"
)

type Event struct {
	Type    string          `json:"type"`
	Payload json.RawMessage `json:"payload"`
}

type EventHandler func(event Event, c *Client) error

const (
	EventSendMessage     = "send_message"
	EventNewMessage      = "new_message"
	EventChangeChat      = "change_chatname"
	EventChangedChatName = "changed_chatname"
)

type SendMessageEvent struct {
	Message string `json:"message"`
	UserID  string `json:"user_id"`
	ChatID  string `json:"chat_id"`
}

type NewMessageEvent struct {
	SendMessageEvent
	Sent time.Time `json:"sent"`
}

type ChangeChatNameEvent struct {
	Name   string `json:"name"`
	ChatID string `json:"chat_id"`
}

type AckEvent struct {
	Name string `json:"name"`
}
