package server

import (
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/jwtauth"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/routes"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/routes/api"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/routes/auth"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/wswebsocket"
)

func SetupRouter(wsManager *wswebsocket.Manager) *gin.Engine {
	router := gin.Default()
	// frontend := getFrontendAssets()
	router.LoadHTMLFiles("dist/index.html")
	router.Use(static.Serve("/", static.LocalFile("dist", false)))

	base := router.Group("")
	base.Use(jwtauth.VerifyJWT())
	base.GET("/ws", wsManager.ServeWS)
	base.GET("/about", routes.IndexHandler)
	base.GET("/ping", routes.PingHandler)

	apiV1 := router.Group("/api/v1")
	apiV1.Use(jwtauth.VerifyJWT())

	// apiV1.GET("/current_user", pingHandler)
	apiV1.GET("/messages/:chat_id/:offset/:limit", api.MessagesHandler)
	// apiV1.GET("/current_user_chats", pingHandler)
	// apiV1.GET("/chat/:chat_id", pingHandler)
	// apiV1.GET("/users/:offset/:limit", pingHandler)
	// apiV1.GET("/user/:user_id", api.UserHandler)
	apiV1.GET("/chats/:offset/:limit", api.ChatsHandler)
	// apiV1.POST("/message/:chat_id", pingHandler)

	authGroup := router.Group("/auth")

	authGroup.POST("/login", auth.LoginHandler)
	authGroup.GET("/logout", auth.LogoutHandler)
	authGroup.GET("/register", routes.IndexHandler)
	authGroup.POST("/register", routes.IndexHandler)

	return router
}
