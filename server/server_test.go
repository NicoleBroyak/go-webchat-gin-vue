package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/nicolebroyak/go-webchat-gin-vue/dbmodels"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/logger"
	"gitlab.com/nicolebroyak/go-webchat-gin-vue/wswebsocket"
	"gopkg.in/go-playground/assert.v1"
)

var (
	wsManager = wswebsocket.NewManager()
	router    = SetupRouter(wsManager)
)

func TestPingRoute(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "pong", w.Body.String())
}

func TestIndexRoute(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 301, w.Code)
}

func TestMessagesRoute(t *testing.T) {
	for i := 1; i < 4; i++ {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", fmt.Sprintf("/api/v1/messages/%d/0/10", i), nil)
		router.ServeHTTP(w, req)
		assert.Equal(t, 200, w.Code)

		body := w.Body.Bytes()

		messages := dbmodels.Messages{}
		json.Unmarshal(body, &messages)

		assert.Equal(t, len(messages) <= 10, true)
		for _, message := range messages {
			assert.Equal(t, uint(i), message.ChatID)
		}
	}
}

func TestChatsRoute(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/chats/0/10", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, 200, w.Code)

	body := w.Body.Bytes()

	chats := dbmodels.Chats{}
	json.Unmarshal(body, &chats)

	logger.L.Info(string(body))
	assert.Equal(t, len(chats) <= 10, true)
	for i, chat := range chats {
		assert.Equal(t, uint(i+1), chat.ID)
	}
}
